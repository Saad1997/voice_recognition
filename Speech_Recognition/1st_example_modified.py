import speech_recognition as sr
import time

#Initializing the speech recognizer
r = sr.Recognizer()

#Declaring the listener source
with sr.Microphone() as source:

   # r.energy_threshold = 3000
    r.adjust_for_ambient_noise(source, duration = 1 ) #check for n
    
    print('-'*30)
    print('-'*30)
    print("\n\nHi !! Say something and I will try to understand what you are talking about !!\n")
    print('-'*30)
    print("\nListening :\n")
    print('-'*30)



    while(1):
        #Initializing the listener 
        text = ""
        try:
            #r.pause_threshold = 0.5
            #r.non_speaking_duration = 0.1
            print("\nspeak")

        
            audio = r.listen(source ,timeout = 10 )#, phrase_time_limit= 5  ) #start microphone
            
            try:
                text = r.recognize_google(audio , language='en-IN') #'de-DE'en-IN #for deutsch you can use 'de-DE' , in that case replace english text with deutsch
                print("\nYou said : {}".format(text))
            except sr.UnknownValueError:
                print("can't recognize , please try again")
            except sr.RequestError: 
                print("no internet, please try again")
            
        except sr.WaitTimeoutError:
            print("Timeout")
      
        
        commands = ['open the door' , 'close the door','start climbing the hill']  
        
        if ((commands[0] in text) and (commands[1] in text)):
             print("Opening and Closing")
             text = ""
            
        elif(commands[0] in text):
             print("Opening")
             text = ""
            
        elif(commands[1] in text):
             print("close")
             text = ""
            
        time.sleep(0.5) 
        
        ###TODO:  try offline speech reocognition with vosk
        