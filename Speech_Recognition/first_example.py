import speech_recognition as sr


#Initializing the speech recognizer
r = sr.Recognizer()

#Declaring the listener source
with sr.Microphone() as source:
    r.energy_threshold = 1000
    r.adjust_for_ambient_noise(source , duration= 0.1)
    print('-'*30)
    print('-'*30)
    print("\n\nHi !! Say something and I will try to understand what you are talking about !!\n")
    print('-'*30)
    print("\nListening :\n")
    print('-'*30)
    while(1):
        #Initializing the listener 
        print("recording")
        audio = r.listen(source)
        print("recorded")
        text = ""
        try:
            text = r.recognize_google(audio ) #'de-DE'
            print("\nYou said : {}".format(text))
        except Exception as e:
            print("can't recognize , please try again")
        #command = ['open the door' , 'jump from the roof']  
        #for c in command:
        #    if(c[0] in text):
        #        print("Opening")
        #    elif(c[1] in text):
        #        print("Jumping")
        command = 'open the door'
        if(command in text):
                print("Opening")

        text = ""
        