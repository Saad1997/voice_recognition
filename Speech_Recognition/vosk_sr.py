import argparse
import os
import queue
import sounddevice as sd
import vosk
import sys

q = queue.Queue()

def int_or_str(text):
    """Helper function for argument parsing."""
    try:
        return int(text)
    except ValueError:
        return text


def callback(indata, frames, time, status):
    """This is called (from a separate thread) for each audio block."""
    if status:
        print(status, file=sys.stderr)
    q.put(bytes(indata))



parser = argparse.ArgumentParser(add_help=False)

parser.add_argument('-l', '--list-devices', action='store_true', 
                    help='show list of audio devices and exit')

args, remaining = parser.parse_known_args()


if args.list_devices:
    print(sd.query_devices())
    parser.exit(0)


parser.add_argument(
    '-d', '--device', type=int_or_str,
    help='input device (numeric ID or substring)')
parser.add_argument(
    '-r', '--samplerate', type=int, help='sampling rate')
args = parser.parse_args(remaining)

print(args)


try:
    if args.samplerate is None:
        device_info = sd.query_devices(args.device, 'input')
        # soundfile expects an int, sounddevice provides a float:
        args.samplerate = int(device_info['default_samplerate'])
    
    model = vosk.Model(lang="en-in") #en-in ,en-us

    
    with sd.RawInputStream(samplerate=args.samplerate, blocksize = 1000, device=args.device, dtype='int16',
                            channels=1, callback=callback):
            print('#' * 80)
            print('Press Ctrl+C to stop the recording')
            print('#' * 80)
        
            rec = vosk.KaldiRecognizer(model, args.samplerate)
            
            while True:
                data = q.get()
                    
                if rec.AcceptWaveform(data):

                    print("have data")
                    print(rec.Result())
                   
                #else:
                 #   print(rec.PartialResult())
    
    
except KeyboardInterrupt:
    print('\nDone')
    parser.exit(0)

except Exception as e:
    parser.exit(type(e).__name__ + ': ' + str(e))